/*
 * Performer.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Marko
 */

#ifndef BODY_H_
#define BODY_H_

#include "Action.h"
#include "Motor.h"

class Body {
public:
	Body(Motor* motor_left, Motor* motor_right);
	virtual ~Body();
	void perform(Action* actions);
private:
	Motor* _motor_left;
	Motor* _motor_right;
};

#endif /* BODY_H_ */
