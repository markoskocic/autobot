/*
 * Action.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Marko
 */

#ifndef ACTION_H_
#define ACTION_H_

#include "string.h"

enum ActionDescription {
	UNKNOWN,
	GOING_FORWARD,
	GOING_BACKWARD,
	GOING_LEFT_SIDE_FORWARD,
	GOING_LEFT_SIDE_BACKWARD,
	GOING_RIGHT_SIDE_FORWARD,
	GOING_RIGHT_SIDE_BACKWARD,
	ROTATING_LEFT,
	ROTATING_RIGHT,
	IDLING

};

class Action {

public:
	Action();
	virtual ~Action();

	bool getLeftMotorForward();
	bool getLeftMotorBackward();
	bool getRightMotorForward();
	bool getRightMotorBackward();
	unsigned long getTimestamp();

	bool equals(Action* actions);

	void goForward();
	void goBackward();
	void leftSideForward();
	void leftSideBackward();
	void rightSideForward();
	void rightSideBackward();
	void rotateLeft();
	void rotateRight();
	void stayIdle();

	ActionDescription description();
	const char* toString();

private:

	bool _leftMotorForward;
	bool _leftMotorBackward;
	bool _rightMotorForward;
	bool _rightMotorBackward;

	ActionDescription _description;

	unsigned long _timestamp;
	void setActionsForBothMotors(bool leftForward, bool leftBackward,
			bool rightForward, bool rightBackward);
};

#endif /* ACTION_H_ */
