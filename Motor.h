/*
 * Motor.h
 *
 *  Created on: Feb 21, 2016
 *      Author: Marko
 */

#ifndef MOTOR_H_
#define MOTOR_H_

class Motor {
public:
	Motor(int motor_pin1, int motor_pin2);
	virtual ~Motor();
	void forward();
	void backward();
	void stop();
	void enable();
	void disable();
private:
	int _motor_pin1;
	int _motor_pin2;
	bool _enabled;
	void setPinValues(int pin1_value, int pin2_value);
};

#endif /* MOTOR_H_ */
