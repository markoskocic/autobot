/*
 * Motor.cpp
 *
 *  Created on: Feb 21, 2016
 *      Author: Marko
 */

#include "Motor.h"
#include "Arduino.h"

Motor::Motor(int motor_pin1, int motor_pin2) {
	_motor_pin1 = motor_pin1;
	_motor_pin2 = motor_pin2;
	_enabled = true;
}

Motor::~Motor() {
}

void Motor::enable() {
	_enabled = true;
}
void Motor::disable() {
	setPinValues(LOW, LOW);
	_enabled = false;
}

void Motor::forward() {
	setPinValues(LOW, HIGH);
}

void Motor::backward() {
	setPinValues(HIGH, LOW);
}

void Motor::stop() {
	setPinValues(LOW, LOW);
}

void Motor::setPinValues(int pin1_value, int pin2_value) {
	if (_enabled) {
		digitalWrite(_motor_pin1, pin1_value);
		digitalWrite(_motor_pin2, pin2_value);
	}
}
