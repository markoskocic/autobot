/*
 * Performer.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Marko
 */

#include "Body.h"

#include "Motor.h"
#include "Arduino.h"
#include "Action.h"

Body::Body(Motor *motor_left, Motor *motor_right) {
	_motor_left = motor_left;
	_motor_right = motor_right;
}

Body::~Body() {
}

void Body::perform(Action* action) {
	// left motor
	if (action->getLeftMotorForward()) {
		_motor_left->forward();
	} else if (action->getLeftMotorBackward()) {
		_motor_left->backward();
	} else {
		_motor_left->stop();
	}

	// right motor
	if (action->getRightMotorForward()) {
		_motor_right->forward();
	} else if (action->getRightMotorBackward()) {
		_motor_right->backward();
	} else {
		_motor_right->stop();
	}
}

