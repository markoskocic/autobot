/*

 */

#include "Environment.h"
#include "Arduino.h"

Environment::Environment() {
	_leftClear = false;
	_rightClear = false;
}

bool Environment::leftClear() {
	return _leftClear;

}

bool Environment::rightClear() {
	return _rightClear;
}

void Environment::scan(int leftObstacleSensorPin, int rightObstacleSensorPin) {
	// check for obstacles
	_leftClear = digitalRead(leftObstacleSensorPin) == 1;
	_rightClear = digitalRead(rightObstacleSensorPin) == 1;
}
