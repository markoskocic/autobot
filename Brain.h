/*

 */
#ifndef BRAIN_H
#define BRAIN_H

#include "Arduino.h"

#include "Action.h"
#include "Environment.h"

class Brain {
public:
	Brain();
	virtual ~ Brain();
	/**
	 * Thinking generates actions (but does not act)
	 */
	Action* think(Environment* environment);
private:
	static const int MAX_ACTIONS = 10;
	static const int MAX_KEEP_GOING_BACK_TIME = 300;
	static const int MAX_KEEP_ROTATING_TIME = 300;
	static const int MAX_IDLING_TIME = 400;

	//circular buffer
	Action _actionQueue[MAX_ACTIONS];
	int _circularIndex;
	int _queueSize;
	Action* doneThinking(Action* newActions, Action* previousActions);
	void queueAction(Action* actions);
	/**
	 * Returns next index in this circular buffer
	 */
	int nextIndex(int index);
	/**
	 * Returns next index in this circular buffer
	 */
	int previousIndex(int index);
	/**
	 * Actions queue size
	 */
	int queueSize();
};

#endif
