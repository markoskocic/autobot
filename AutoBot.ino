#include "Body.h"
#include "Environment.h"
#include "Brain.h"
#include "Motor.h"
#include "Logger.h"

#define LOG_LEVEL LOG_LEVEL_DEBUG

const int DISTANCE_SENSOR_L_PIN_OUT = 2;
const int DISTANCE_SENSOR_R_PIN_OUT = 4;

const int PIN_MOTOR_L_EN_OUT = 12;
const int PIN_MOTOR_R_EN_OUT = 8;

const int PIN_MOTOR_L1_OUT = 6;
const int PIN_MOTOR_L2_OUT = 3;
const int PIN_MOTOR_R1_OUT = 11;
const int PIN_MOTOR_R2_OUT = 10;

const int PIN_LED13_OUT = 13;

const int STARTUP_BLINK_DELAY = 500;
const int STARTUP_NUMBER_OF_BLINKS = 10;

Environment environment;
Brain brain;
Motor motor_left = Motor(PIN_MOTOR_L1_OUT, PIN_MOTOR_L2_OUT);
Motor motor_right = Motor(PIN_MOTOR_R1_OUT, PIN_MOTOR_R2_OUT);
Body body = Body(&motor_left, &motor_right);

void setup() {
	// put your setup code here, to run once:

	// initialize serial communication at 9600 bits per second:
	logger.init(LOG_LEVEL, 9600);
	// initialize outputs
	logger.info("Initializing outputs...");
	pinMode(PIN_LED13_OUT, OUTPUT);
	pinMode(PIN_MOTOR_L_EN_OUT, OUTPUT);
	pinMode(PIN_MOTOR_R_EN_OUT, OUTPUT);

	// initialize inputs
	logger.info("Initializing inputs...");
	pinMode(DISTANCE_SENSOR_L_PIN_OUT, INPUT);
	pinMode(DISTANCE_SENSOR_R_PIN_OUT, INPUT);
	logger.info("Enabling motors...");
	enableMotors();
	blinkOnStartup();
	logger.info("Started!");
}

void loop() {

	// scan environment
	environment.scan(DISTANCE_SENSOR_L_PIN_OUT, DISTANCE_SENSOR_R_PIN_OUT);

	// think
	Action* action = brain.think(&environment);

	// act
	body.perform(action);

}

void ledOn() {
	digitalWrite(PIN_LED13_OUT, HIGH);
}

void ledOff() {
	digitalWrite(PIN_LED13_OUT, LOW);
}

void enableMotors() {
	digitalWrite(PIN_MOTOR_L_EN_OUT, HIGH);
	digitalWrite(PIN_MOTOR_R_EN_OUT, HIGH);
}

void blinkOnStartup() {
	int blink_decrement = STARTUP_BLINK_DELAY / STARTUP_NUMBER_OF_BLINKS;
	for (int i = 0; i < STARTUP_NUMBER_OF_BLINKS; i++) {
		ledOn();
		delay(STARTUP_BLINK_DELAY);
		ledOff();
		delay(STARTUP_BLINK_DELAY - i * blink_decrement);
		ledOn();
	}
}
