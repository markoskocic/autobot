/*
 * Action.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Marko
 */

#include "Brain.h"
#include "Arduino.h"

#include "Action.h"
#include "Logger.h"
#include "string.h"

Brain::Brain() {
	_circularIndex = 0;
	_queueSize = 0;
}

Brain::~Brain() {
}

Action* Brain::think(Environment* environment) {
	Action newAction;
	Action* lastAction = 0;
	unsigned long currentTime = millis();
	if (queueSize() > 0) {
		lastAction = &_actionQueue[previousIndex(_circularIndex)];

		if (IDLING == lastAction->description()) {
			//stay idle for at least MAX_IDLING_TIME
			if (currentTime <= lastAction->getTimestamp() + MAX_IDLING_TIME) {
				newAction.stayIdle();
				return doneThinking(&newAction, lastAction);
			} else {
				// back from idling, time to move
				// check if previousToLastAction was go back - in that case rotate either left or right
				if (queueSize() > 0) {
					Action* previousToLastAction = &_actionQueue[previousIndex(
							previousIndex(_circularIndex))];
					if (GOING_BACKWARD == previousToLastAction->description()) {
						switch (random(2)) {
						case 0:
							newAction.rotateLeft();
							break;
						default:
							newAction.rotateRight();
							break;
						}
						return doneThinking(&newAction, lastAction);
					}
				}

				// if both sides are clear, go forward
				if (environment->leftClear() && environment->rightClear()) {
					newAction.goForward();
				}
				// if left clear, but right obstacle, then rightBackward
				else if (environment->leftClear()
						&& !environment->rightClear()) {
					newAction.rightSideBackward();
				}
				// if right clear, but left obstacle, then leftBackward
				else if (environment->rightClear()
						&& !environment->leftClear()) {
					newAction.leftSideBackward();
				}
				//if both sides show obstacles go back
				else if (!environment->leftClear()
						&& !environment->rightClear()) {
					newAction.goBackward();
				}

				return doneThinking(&newAction, lastAction);
			}
		}
		// if robot was going backward, maybe it should keep going
		else if (GOING_BACKWARD == lastAction->description()) {
			//keep going for at least MAX_KEEP_GOING_BACK_TIME
			if (currentTime
					<= lastAction->getTimestamp() + MAX_KEEP_GOING_BACK_TIME) {
				newAction.goBackward();
				return doneThinking(&newAction, lastAction);
			} else {
				// change expected, so stay for a while
				newAction.stayIdle();
				return doneThinking(&newAction, lastAction);
			}
		}
		// if robot was rotating left, maybe it should keep rotating left
		else if (ROTATING_LEFT == lastAction->description()) {
			//keep rotating for at least MAX_KEEP_ROTATING_TIME
			if (currentTime
					<= lastAction->getTimestamp() + MAX_KEEP_ROTATING_TIME) {
				newAction.rotateLeft();
				return doneThinking(&newAction, lastAction);
			} else {
				// change expected, so stay for a while
				newAction.stayIdle();
				return doneThinking(&newAction, lastAction);
			}
		}
		// if robot was rotating right, maybe it should keep rotating right
		else if (ROTATING_RIGHT == lastAction->description()) {
			//keep rotating for at least MAX_KEEP_ROTATING_TIME
			if (currentTime
					<= lastAction->getTimestamp() + MAX_KEEP_ROTATING_TIME) {
				newAction.rotateRight();
				return doneThinking(&newAction, lastAction);
			} else {
				// change expected, so stay for a while
				newAction.stayIdle();
				return doneThinking(&newAction, lastAction);
			}
		}

	}

	// if both sides are clear, go forward
	if (environment->leftClear() && environment->rightClear()) {
		// in first or same
		if (queueSize() == 0 || lastAction->description() == GOING_FORWARD) {
			newAction.goForward();
		} else {
			newAction.stayIdle();
		}
	}
	// if left clear, but right obstacle, then rightBackward
	else if (environment->leftClear() && !environment->rightClear()) {
		// in first or same
		if (queueSize() == 0
				|| lastAction->description() == GOING_RIGHT_SIDE_BACKWARD) {
			newAction.rightSideBackward();
		} else {
			newAction.stayIdle();
		}
	}
	// if right clear, but left obstacle, then leftBackward
	else if (environment->rightClear() && !environment->leftClear()) {
		// in first or same
		if (queueSize() == 0
				|| lastAction->description() == GOING_LEFT_SIDE_BACKWARD) {
			newAction.leftSideBackward();
		} else {
			newAction.stayIdle();
		}
	}
	//if both sides show obstacles go back
	else if (!environment->leftClear() && !environment->rightClear()) {
		// in first or same
		if (queueSize() == 0 || lastAction->description() == GOING_BACKWARD) {
			newAction.goBackward();
		} else {
			newAction.stayIdle();
		}

	}
	return doneThinking(&newAction, lastAction);
}

Action * Brain::doneThinking(Action * newAction, Action * lastAction) {

	// check if this set of actions is different then previous
	if (lastAction == 0 || !newAction->equals(lastAction)) {
		//insert only if it is the first or different then last
		queueAction(newAction);
		logger.debug("New action: %s", newAction->toString());
	}
	//always return latest action
	return &_actionQueue[_circularIndex - 1];
}

void Brain::queueAction(Action* action) {
	if (queueSize() < MAX_ACTIONS) {
		_queueSize++;
	}
	// insert new element into circular buffer
	_actionQueue[_circularIndex] = *action;

	// increment index
	_circularIndex = nextIndex(_circularIndex);

}

int Brain::nextIndex(int index) {
	if (++index >= MAX_ACTIONS) {
		return 0;
	} else {
		return index;
	}
}

int Brain::previousIndex(int index) {
	if (queueSize() == 0) {
		return -1;
	}
	if (--index < 0) {
		return 0;
	} else {
		return index;
	}
}

int Brain::queueSize() {
	return _queueSize;
}

