/*

*/
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "Arduino.h"

class Environment
{
  public:
	Environment();
	void scan(int leftObstacleSensorPin, int rightObstacleSensorPin);
	bool leftClear();
	bool rightClear();
  private:
    bool _leftClear;
    bool _rightClear;
};

#endif
