/*
 * Action.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Marko
 */

#include "Action.h"
#include "string.h"
#include "Arduino.h"

Action::Action() {
	_leftMotorForward = false;
	_leftMotorBackward = false;
	_rightMotorForward = false;
	_rightMotorBackward = false;
	_timestamp = millis();
	_description = UNKNOWN;
}

Action::~Action() {
}

/**
 * Two actions are considered equal if all fields except timestamp are equal
 */
bool Action::equals(Action* actions) {
	return _leftMotorBackward == actions->_leftMotorBackward
			&& _leftMotorForward == actions->_leftMotorForward
			&& _rightMotorBackward == actions->_rightMotorBackward
			&& _rightMotorForward == actions->_rightMotorForward;
}

bool Action::getLeftMotorForward() {
	return _leftMotorForward;
}
bool Action::getLeftMotorBackward() {
	return _leftMotorBackward;
}
bool Action::getRightMotorForward() {
	return _rightMotorForward;
}
bool Action::getRightMotorBackward() {
	return _rightMotorBackward;
}
unsigned long Action::getTimestamp() {
	return _timestamp;
}
ActionDescription Action::description() {
	return _description;
}

const char* Action::toString() {
	switch (_description) {
	case GOING_FORWARD:
		return "GOING_FORWARD";
	case GOING_BACKWARD:
		return "GOING_BACKWARD";
	case GOING_LEFT_SIDE_FORWARD:
		return "GOING_LEFT_SIDE_FORWARD";
	case GOING_LEFT_SIDE_BACKWARD:
		return "GOING_LEFT_SIDE_BACKWARD";
	case GOING_RIGHT_SIDE_FORWARD:
		return "GOING_RIGHT_SIDE_FORWARD";
	case GOING_RIGHT_SIDE_BACKWARD:
		return "GOING_RIGHT_SIDE_BACKWARD";
	case ROTATING_LEFT:
		return "ROTATING_LEFT";
	case ROTATING_RIGHT:
		return "ROTATING_RIGHT";
	case IDLING:
		return "IDLING";
	default:
		return "UNKNOWN";

	}
	return "UNKNOWN";
}

void Action::goForward() {
	_description = GOING_FORWARD;
	setActionsForBothMotors(true, false, true, false);
}
void Action::goBackward() {
	_description = GOING_BACKWARD;
	setActionsForBothMotors(false, true, false, true);
}
void Action::leftSideForward() {
	_description = GOING_LEFT_SIDE_FORWARD;
	setActionsForBothMotors(true, false, false, false);
}
void Action::leftSideBackward() {
	_description = GOING_LEFT_SIDE_BACKWARD;
	setActionsForBothMotors(false, true, false, false);
}
void Action::rightSideForward() {
	_description = GOING_RIGHT_SIDE_FORWARD;
	setActionsForBothMotors(false, false, true, false);
}
void Action::rightSideBackward() {
	_description = GOING_RIGHT_SIDE_BACKWARD;
	setActionsForBothMotors(false, false, false, true);
}
void Action::rotateLeft() {
	_description = ROTATING_LEFT;
	setActionsForBothMotors(false, true, true, false);
}
void Action::rotateRight() {
	_description = ROTATING_RIGHT;
	setActionsForBothMotors(true, false, false, true);
}
void Action::stayIdle() {
	_description = IDLING;
	setActionsForBothMotors(false, false, false, false);
}

void Action::setActionsForBothMotors(bool leftForward, bool leftBackward,
		bool rightForward, bool rightBackward) {
	_leftMotorForward = leftForward;
	_leftMotorBackward = leftBackward;
	_rightMotorForward = rightForward;
	_rightMotorBackward = rightBackward;
}
